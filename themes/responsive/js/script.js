function min_height_body(){
    var h = $(window).innerHeight();
    $('.wrapper').css('min-height', h);
}
var slider = $('#slider');
$(document).ready(function(){
    min_height_body();
   $('#mb-menu').click(function(){
        $('#main-menu').slideToggle(300);
    });
    if($('#slider').length){


        slider.on('init', function(){
            var target = $('#slider div:first-child');
            if(target.find('video').length){
                video = target.find('video').get(0);
                var playPromise = video.play();
                if(/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())){
                }else{
                    $(video).prop('muted', false);
                    if (playPromise !== undefined) {
                        playPromise.then(_ => {
                        }).catch(error => {
                        });
                    }
                }

            }
        });
        slider = slider.slick({
            autoplay: false,
            autoplaySpeed: 10000,
            arrows: true,
            pauseOnHover: false,
        });

        slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            if($('.slick-slide video').length){
                $('.slick-slide video').each(function(){
                     $(this).get(0).pause();
                });
            }
        });
        slider.on('afterChange', function(event, slick, currentSlide, nextSlide){
            var target = $('.slick-current');
            if(target.find('video').length){
                slider.slick('slickPause');
                video = target.find('video').get(0);
                $(video).prop('muted', false);
                video.play();
                $(video).on("ended", function() {
                    slider.slick('slickPlay');
                });
            }
        });
    }    
});
function goNextSlide(){
    (function($) {
        slider.slick('slickPlay');
        slider.slick('slickNext');
    })(jQuery);
}