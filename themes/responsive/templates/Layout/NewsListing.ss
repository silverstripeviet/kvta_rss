<div class="main-content clearfix">	
    <div class="col-xs-12 col-md-8 news listing">
		$Content
        <% loop $getListing %>
			<div class="row">
				<div class="col-xs-12 col-md-8 box">
					<div class="image">
						<a href="$Link">
							<img src="$ThumbnailImage.CroppedImage(730,424).URL" alt="$MenuTitle" width="500" />
						</a>
					</div>
				</div>
				<div class="col-xs-12 col-md-4 box">
					<h3 class="story-title"><a href="$Link">$Title</a></h3>
					<p class="date">$Date.Format('n-d-Y')</p>
					<p class="summary"><% if $Summary %>$Summary<% else %>$Content.LimitWordCount(15)<% end_if %></p>
					<p class="read-more"><a href="$Link">CLICK HERE FOR THE FULL NOTE</a></p>
				</div>
			</div>
        <% end_loop %>
		<% if $getListing.MoreThanOnePage %>
			<div class="row text-center">
				<ul class="pagination PageNumbers">
					<% if $getListing.NotFirstPage %>
						<li class="prev">
							<a class="paginate-left" href="$getListing.PrevLink" title="View the previous page">Prev</a>
						</li>
					<% else %>	
						<li class="prev disabled">
							<a class="paginate-left disabled">Prev</a>
						</li>
					<% end_if %>
				
					<% loop $getListing.PaginationSummary(4) %>
						<% if CurrentBool %>
							<li class="active"><a class="disabled">$PageNum</a></li>
						<% else %>
							<% if Link %>
								<li>
									<a class="<% if BeforeCurrent %>paginate-left<% else %>paginate-right<% end_if %>" href="$Link">
									$PageNum
									</a>
								</li>
							<% else %>
								<li class="disabled"><a class="disabled">&hellip;</a></li>						
							<% end_if %>
						<% end_if %>
					<% end_loop %>
				
					<% if $getListing.NotLastPage %>
						<li class="next">
							<a class="next paginate-right" href="$getListing.NextLink" title="View the next page">Next</a>
						</li>
					<% else %>
						<li class="next disabled">
							<a class="next paginate-right disabled">Next</a>
						</li>
					<% end_if %>
				</ul>
			</div>
			<% end_if %>
    </div>
    <div class="col-xs-12 col-md-4 socials">
        <% include RightSidebar %>
    </div>
</div>