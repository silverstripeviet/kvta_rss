<% include SlideHome %>
<div class="main-content clearfix">
    <div class="col-xs-12 col-md-8 news">
        <% loop $getLatestNews(8) %>
            <div class="col-xs-12 col-md-6 box">
                <div class="image">
                    <a href="$Link">
                        <img src="$ThumbnailImage.CroppedImage(730,424).URL" alt="$MenuTitle" width="365" />
                    </a>
                </div>
                <% if $Top.SiteConfig.ShowStoryTitle %>
                    <p class="story-title"><a href="$Link">$Title.LimitWordCount(12)</a></p>
                <% end_if %>
            </div>
        <% end_loop %>
    </div>
    <div class="col-xs-12 col-md-4 socials">
        <% include RightSidebar %>
    </div>
</div>