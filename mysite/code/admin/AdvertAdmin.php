<?php

class AdvertAdmin extends ModelAdmin {
    public static $managed_models = array('Advert');
    static $url_segment = 'adverts';
    static $menu_title = 'Adverts';

    public function getEditForm($id = null, $fields = null) {
        $form = parent::getEditForm($id, $fields);
        return $form;
    }
}