<?php
class NewsPage extends Page{
    static $db = array(
        'Author' => 'Varchar(255)',
        'Date' => 'Date',
		'Summary' => 'Text',
    );
    
	static $default_sort = 'Date DESC';
	
    static $has_one = array(
        'ThumbnailImage' => 'Image'
    );
    
    function getCMSFields(){
        $f = parent::getCMSFields();
		$f->addFieldToTab('Root.Main', TextareaField::create('Summary'), 'Content');
        $f->addFieldsToTab('Root.Info', array(
            new TextField('Author'),
            new DateField('Date'),
            new UploadField('ThumbnailImage', 'Thumbnail image'),
        ));		
        return $f;
        
    }
	
	function stringDate(){
		$date = $this->Date;
		$date_ = new Date();
        $date_->setValue($date);
		$date_string = htmlentities($date_->FormatI18N('%A %B %d,%Y'));
		return $date_string;
	}
}

class NewsPage_Controller extends Page_Controller{
    
}