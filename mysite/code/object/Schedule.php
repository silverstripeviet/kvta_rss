<?php
class Schedule extends DataObject{
    static $db = array(
        'Title' => 'Varchar(255)',
        'Link' => 'Text',
        'TimeFrom' => 'Int',
        'TimeTo' => 'Int',
        'Day' => 'Int'
    );
    
    static $has_one = array(
        'ScheduleImage' => 'Image',
    );
    
    function getCMSFields(){
        $timeRange = array();
        for($i = 1; $i <= 24; $i++){
            $timeRange[$i] = $i;
        }
        $dayRange = array(
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday',
        );
        
        return new FieldList(
            new TextField('Title'),            
            new UploadField('ScheduleImage', 'Schedule image (width: 350px)'),
            new DropdownField('TimeFrom', 'From hour', $timeRange),
            new DropdownField('TimeTo', 'To hour', $timeRange),
            new DropdownField('Day', 'Day', $dayRange),
            new TextField('Link', 'Link')            
        );
    }
    
    function revertDay(){
        $dayRange = array(
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            0 => 'Sunday',
        );
        if(isset($dayRange[$this->Day])){
            return $dayRange[$this->Day];
        }
    }
    
    function summaryFields(){
        return array(
            'Title' => 'Title',
            'ScheduleImage.CMSThumbnail' => 'ON AIR',
            'TimeFrom' => 'From hour',
            'TimeTo' => 'To hour',
            'revertDay' => 'Day',
            'Link' => 'Link',
        );
    }
}