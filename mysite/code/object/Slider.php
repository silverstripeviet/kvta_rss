<?php
class Slider extends DataObject{
    static $db = array(
        'Link' => 'Text',
        'SortOrder' => 'Int',
        'PublishOnDate' => 'SS_Datetime',
        'UnPublishOnDate' => 'SS_Datetime',
        'SlideType' => "Enum('Image,UploadVideo,Embed','Image')",
        'EmbedVideo' => 'Text',
    );
    
    static $default_sort = "SortOrder";
    
    static $has_one = array(
        'SlideImage' => 'Image',
        'Video' => 'File',
        'Page' => 'Page',
    );
    
    function getCMSFields(){
        Requirements::css('embargoexpiry/thirdparty/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.css');
		Requirements::css('embargoexpiry/css/WorkflowCMS.css');
		Requirements::javascript('embargoexpiry/thirdparty/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.js');
		Requirements::javascript('mysite/js/ObjectTimePicker.js');
        $PublishOnDate = Datetimefield::create('PublishOnDate', _t('WorkflowEmbargoExpiryExtension.PUBLISH_ON', 'Scheduled publish date'));
        $UnPublishOnDate = Datetimefield::create('UnPublishOnDate', _t('WorkflowEmbargoExpiryExtension.UNPUBLISH_ON', 'Scheduled un-publish date'));
        $PublishOnDate->getDateField()->setConfig('showcalendar', true);
        $PublishOnDate->getTimeField()->setConfig('timeformat', 'HH:mm');
        $PublishOnDate->getTimeField()->addExtraClass('hasTimePicker');
        
		$UnPublishOnDate->getDateField()->setConfig('showcalendar', true);		
		$UnPublishOnDate->getTimeField()->setConfig('timeformat', 'HH:mm');        
		$UnPublishOnDate->getTimeField()->addExtraClass('hasTimePicker');

		$slideType = array(
		    'Image' => 'Image',
            'UploadVideo' => 'Upload video',
            'Embed' => 'Embed video',
        );
        return new FieldList(
            OptionsetField::create('SlideType', 'Slide type', $slideType),
            TextField::create('Link'),
            UploadField::create('SlideImage', 'Slide image')->setDescription('Dimension: 1140 x 636 for HomePage, 710 x 396 for Inner page'),
            UploadField::create('Video', 'Upload MP4 video'),
            TextareaField::create('EmbedVideo', 'Embed video')->setDescription('Iframe dimension: 1140 x 636 for HomePage, 710 x 396 for Inner page'),
            $PublishOnDate,
			$UnPublishOnDate
        );
    }
    
    function summaryFields(){
        return array(
            'Link' => 'Link',
            'SlideImage.CMSThumbnail' => 'Image',
            'PublishOnDate' => 'Publish Date',
            'UnPublishOnDate' => 'Expire Date',
        );
    }
}