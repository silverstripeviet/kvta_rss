<?php
class SidebarImage extends DataObject{
    static $db = array(
        'Name' => 'Varchar(255)',
        'Link' => 'Text',
    );

    static $has_one = array(
        'Image' => 'Image',
    );

    function getCMSFields(){
        return new FieldList(
            new TextField('Name'),
            new TextField('Link', 'Link'),
            new UploadField('Image', 'Image (width: 300px)')
        );
    }

    function summaryFields(){
        return array(
            'Name' => 'Name',
            'Link' => 'Link',
            'Image.CMSThumbnail' => 'Image',
        );
    }
}