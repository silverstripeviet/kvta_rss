<?php
class Advert extends DataObject{
    static $db = array(
        'Title' => 'Varchar(255)',
        'ADScript' => 'Text',
        'SortOrder' => 'Int',
        'Link' => 'Text',
        'Type' => "Enum('Script,Image','Script')",     
        'PublishOnDate' => 'SS_Datetime',
        'UnPublishOnDate' => 'SS_Datetime',   
    );
    
    static $default_sort = "SortOrder";
    
    static $has_one = array(
        'ImageAD' => 'Image',
        'Page' => 'Page',
    );
    
    function getCMSFields(){
        $type = array(
            'Script' => 'Embed AD script',
            'Image' => 'Image AD',
        );
        Requirements::css('embargoexpiry/thirdparty/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.css');
		Requirements::css('embargoexpiry/css/WorkflowCMS.css');
		Requirements::javascript('embargoexpiry/thirdparty/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.js');
		Requirements::javascript('mysite/js/ObjectTimePicker.js');
        $PublishOnDate = Datetimefield::create('PublishOnDate', _t('WorkflowEmbargoExpiryExtension.PUBLISH_ON', 'Scheduled publish date'));
        $UnPublishOnDate = Datetimefield::create('UnPublishOnDate', _t('WorkflowEmbargoExpiryExtension.UNPUBLISH_ON', 'Scheduled un-publish date'));
        $PublishOnDate->getDateField()->setConfig('showcalendar', true);
        $PublishOnDate->getTimeField()->setConfig('timeformat', 'HH:mm');
        $PublishOnDate->getTimeField()->addExtraClass('hasTimePicker');
        
		$UnPublishOnDate->getDateField()->setConfig('showcalendar', true);		
		$UnPublishOnDate->getTimeField()->setConfig('timeformat', 'HH:mm');        
		$UnPublishOnDate->getTimeField()->addExtraClass('hasTimePicker');
        $ADScript = new TextareaField('ADScript', 'AD script');
        $ADScript->setRows(13);
        return new FieldList(
            new OptionsetField('Type', 'Choose advert type', $type),
            new TextField('Title'),
            new HeaderField('ForEmbedScript', 'For embed script AD'),
            $ADScript,
            new HeaderField('ForImageAD', 'For image AD'),
            new UploadField('ImageAD', 'AD image (width 350px)'),
            new TextField('Link', 'Link'),
            $PublishOnDate,
			$UnPublishOnDate
        );
    }
    
    function summaryFields(){
        return array(
            'Type' => 'Type',
            'Title' => 'Title',
            'ImageAD.CMSThumbnail' => 'AD image',
            'Link' => 'Link',
        );
    }
}