<?php
class NewsListing extends Page{
	private static $allowed_children = array('NewsPage', 'MediaPage');
}
class NewsListing_Controller extends Page_Controller{
	function getListing() {
		$entries = Page::get()->filter('ParentID', $this->ID);
        $list = new PaginatedList($entries, Controller::curr()->request);
    	$list->setPageLength(4);
    	return $list;
	}
}