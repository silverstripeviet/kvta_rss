<?php

class HomePage extends Page {

    public static $db = array();
    public static $has_one = array(

    );
	
	public static $many_many = array(
		'FeaturedStories' => 'Page'
	);
	
	public static $many_many_extraFields = array(
        'FeaturedStories'  =>  array('SortOrder'=>'Int')
    );

    function getCMSFields() {

        $fields = parent::getCMSFields();
		$featured_stories_config = new GridFieldConfig_RelationEditor();
		$featured_stories_config->addComponent(new GridFieldSortableRows('SortOrder'));
		$featured_stories_config->removeComponentsByType(new GridFieldAddNewButton());
		$featured_stories_grid = new GridField('FeaturedStories', 'Featured Stories', $this->FeaturedStories(), $featured_stories_config);
		
		$fields->addFieldsToTab('Root.FeaturedStories', $featured_stories_grid);
        return $fields;
    }

}

class HomePage_Controller extends Page_Controller {

    private static $allowed_actions = array(
    );
	
	function getLatestFeaturedStories($limit=4){
		return $this->FeaturedStories()->sort('SortOrder')->limit($limit);
	}
    
    function getLatestNews($limit=5){
        return NewsPage::get()->limit($limit);
    }
    
    function getLatestMedias($limit=5){
        return MediaPage::get()->limit($limit);
    }
}
