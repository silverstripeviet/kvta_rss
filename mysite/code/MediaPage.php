<?php
class MediaPage extends Page{
    static $db = array(
        'Author' => 'Varchar(255)',
        'Date' => 'Date',
		'Summary' => 'Text',
    );
	
	static $default_sort = 'Date DESC';
    
    static $has_one = array(
        'ThumbnailImage' => 'Image'
    );
    
    function getCMSFields(){
        $f = parent::getCMSFields();
		$f->addFieldToTab('Root.Main', TextareaField::create('Summary'), 'Content');
        $f->addFieldsToTab('Root.Info', array(
            new TextField('Author'),
            new DateField('Date'),
            new UploadField('ThumbnailImage', 'Thumbnail image'),
        ));
        return $f;
        
    }
}

class MediaPage_Controller extends Page_Controller{
    
}