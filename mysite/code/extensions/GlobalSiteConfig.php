<?php

class GlobalSiteConfig extends DataExtension {

    public static $db = array(
        'Footer' => 'HTMLText',
        'ListenNowLink' => 'Text',
        'ShowStoryTitle' => 'Boolean',
        'ADScript1' => 'Text',
        'ADScript2' => 'Text',
        'GA' => 'Text',
        'FacebookEmbed' => 'Text',
        'TwitterEmbed' => 'Text',
        'FacebookPixel' => 'Text',
    );
    public static $has_one = array(
        'Logo' => 'Image',
        'ListenNowImage' => 'Image',
        'PageBackgroundImage' => 'Image',
    );

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldsToTab('Root.Main', array(
            new CheckboxField('ShowStoryTitle', 'Show story title in all Listing'),
            new UploadField('Logo', 'Logo(167x111)'),
            new UploadField('PageBackgroundImage', 'Page background image'),
            $GA = new TextareaField('GA', 'Google analytic script'),
            $FacebookPixel = new TextareaField('FacebookPixel', 'Facebook Pixel'),
        ));        
        $GA->setRows(12);
        $FacebookPixel->setRows(12);

        $SidebarConfig = GridFieldConfig_RecordEditor::create();
        $sidebarGrid = new GridField('SidebarImage', 'Sidebar Image', SidebarImage::get(), $SidebarConfig);
        $fields->addFieldsToTab('Root.Social', array(
            $fb = new TextareaField('FacebookEmbed', 'Facebook embed iframe'),
            $tw = new TextareaField('TwitterEmbed', 'Twitter embed iframe'),
            $sidebarGrid,
        ));
        $fb->setRows(9);
        $tw->setRows(9);
        
        $fields->addFieldsToTab('Root.Header', array(
            new UploadField('ListenNowImage', 'Listen Now button image (216x88)'),
            new TextField('ListenNowLink', 'Listen Now button link'),
        ));
        
        $fields->addFieldsToTab('Root.Footer', array(
            $Footer = new HtmlEditorField('Footer', 'Footer')
        ));
        $Footer->setRows(10);
        
        $scheduleConfig = GridFieldConfig_RecordEditor::create();
        $scheduleGrid = new GridField('Schedule', 'Schedule', Schedule::get(), $scheduleConfig);        
        $fields->addFieldToTab('Root.Schedule', $scheduleGrid);    
    }

}
