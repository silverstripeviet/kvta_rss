<?php

class Page extends SiteTree {

    private static $db = array(
        'ShowFBFeed' => 'Boolean',
        'ShowTWFeed' => 'Boolean',
        'ShowSidebarImages' => 'Boolean',
        'HideAds' => 'Boolean',
    );
    
    private static $has_one = array(
        'PageBackgroundImage' => 'Image',
    );
    
    private static $has_many = array(
        'Adverts' => 'Advert',
    );
    
    private static $many_many = array(
        'Sliders' => 'Slider'
    );

    public function getCMSFields() {

        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Background', array(
            new UploadField('PageBackgroundImage', 'Page background image'),
        ));
        
        $sliderConfig = GridFieldConfig_RecordEditor::create();
        $sliderConfig->addComponent(new GridFieldOrderableRows('SortOrder'));
        $sliderConfig->addComponent(new GridFieldBulkUpload());
        $sliderGrid = new GridField('Sliders', 'Slider', $this->Sliders(), $sliderConfig);        
        $fields->addFieldToTab('Root.Slides', $sliderGrid);
        
        $fields->addFieldsToTab('Root.Sidebar', array(
            new CheckboxField('ShowFBFeed', 'Show Facebook feeds'),
            new CheckboxField('ShowTWFeed', 'Show Twitter feeds'),
            new CheckboxField('HideAds', 'Hide ads'),
        ));

        return $fields;
    }

}

class Page_Controller extends ContentController {
    
    function getSchedule(){
        $now_h = date('G') * 1;
        $day = date('N') * 1;
        #print_r('day = ' . $day . ', h = ' . $now_h . '<br />');
        $schedule = Schedule::get()->filter(array(
        	'TimeFrom:LessThanOrEqual' => $now_h,
        	'TimeTo:GreaterThan' => $now_h,
            'Day' => $day,
        	))->first();
        return $schedule;              
    }
    
    function getSlides(){
        $results = new ArrayList();
        $slides = $this->Sliders();
        if($slides){
            foreach($slides as $slide){
                if(strtotime($slide->PublishOnDate) < time() && strtotime($slide->UnPublishOnDate) > time()){
                    $results->push($slide);
                }
            }
            return $results;
        }
    }
    
    function getValidAdverts(){
        $results = new ArrayList();
        $adverts = Advert::get();
        if($adverts){
            foreach($adverts as $advert){
                if($advert->Type == 'Script'){
                    $results->push($advert);
                }else{
                    if(strtotime($advert->PublishOnDate) < time() && strtotime($advert->UnPublishOnDate) > time()){
                        $results->push($advert);
                    }
                }
            }
            return $results;
        }
    }

    function getSidebarImages(){
        return SidebarImage::get();
    }
}
