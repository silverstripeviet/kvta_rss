<?php
require_once('conf/ConfigureFromEnv.php');
global $project;
$project = 'mysite';


date_default_timezone_set('America/Los_Angeles'); 

setlocale(LC_ALL, "es_ES");
DateField::set_default_config('setLocale','es');

DateField::set_default_config('showcalendar', true);

MySQLDatabase::set_connection_charset('utf8');
SiteConfig::add_extension('GlobalSiteConfig');
SSViewer::set_theme('responsive');

// enable nested URLs for this site (e.g. page/sub-page/)
SiteTree::enable_nested_urls();
HtmlEditorConfig::get('cms')->insertButtonsAfter('tablecontrols', 'fontsizeselect', 'forecolor', 'backcolor');
FulltextSearchable::enable(array('SiteTree'));

if (Director::isDev()) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
} 
