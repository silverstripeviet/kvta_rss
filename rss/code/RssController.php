<?php

class RSSController extends ContentController {

    private static $allowed_actions = array(
        'index'
    );

    public function init() {
        parent::init();
        RSSFeed::linkToFeed($this->Link() . "rss", "5 Most Recently News");
    }

    public function index() {
        $rss = new RSSFeed(
                $this->LatestUpdates(),
                $this->Link(),
                "5 Most Recently News",
                "Shows a list of the 5 most recently news."
        );
        $rss->setTemplate('Rss');

        return $rss->outputToBrowser();
    }

    public function LatestUpdates() {
        return NewsPage::get()->sort("Date", "DESC")->limit(5);
    }

}
